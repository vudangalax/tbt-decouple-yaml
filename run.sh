CLUSTER_NAME=tbtdecouplecluster

gcloud container clusters create $CLUSTER_NAME --num-nodes=1 --enable-autoupgrade --no-enable-basic-auth --no-issue-client-certificate --enable-ip-alias --metadata disable-legacy-endpoints=true	

gcloud container clusters get-credentials $CLUSTER_NAME		

brew install helm		

helm init

kubectl create serviceaccount --namespace kube-system tiller

kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

kubectl patch deploy --namespace kube-system tiller-deploy -p '{""spec"":{""template"":{""spec"":{""serviceAccount"":""tiller""}}}}'
helm init --service-account tiller --upgrade

## right here waiting pod tiller running"	

helm install stable/nginx-ingress --name nginx-ingress --set controller.publishService.enabled=true

# create volumne
kubectl apply -f ssd-storageclass.yaml
kubectl apply -f tbtdecouple-volumeclaim.yaml
echo 'create ssd success'

# create ssl and ip address
#kubectl apply -f compute-address.yaml
# gcloud compute addresses create tbtdecouple-ip-address --global

# kubectl apply -f tbtdecouple-certificate.yaml
echo 'create public ip address and ssl success'

#deploy new container
kubectl create -f tbtdecouple.yaml

# create basic auth 
./createBasicAuthentication.sh

# # #create ingress
# kubectl apply -f tbtdecouple-ingress.yaml
# echo 'create ingress'

kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml --validate=false

kubectl create namespace cert-manager

helm repo add jetstack https://charts.jetstack.io

helm install --name cert-manager --version v0.12.0 --namespace cert-manager jetstack/cert-manager

# apply ssl to ingress -->     cert-manager.io/cluster-issuer: letsencrypt-prod and add tls
kubectl apply -f tbtdecouple-ingress.yaml

kubectl apply -f production_issuer.yaml



# echo 'watch get pod ...'
# kubectl get pod -l app=tbtdecouple --watch